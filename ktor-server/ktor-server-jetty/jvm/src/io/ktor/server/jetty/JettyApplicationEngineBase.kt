/*
 * Copyright 2014-2019 JetBrains s.r.o and contributors. Use of this source code is governed by the Apache 2.0 license.
 */

package io.ktor.server.jetty

import io.ktor.server.application.*
import io.ktor.server.engine.*
import kotlinx.coroutines.*
import org.eclipse.jetty.server.*
import java.util.concurrent.*

/**
 * [ApplicationEngine] base type for running in a standalone Jetty
 */
public open class JettyApplicationEngineBase(
    environment: ApplicationEngineEnvironment,
    configure: Configuration.() -> Unit
) : BaseApplicationEngine(environment) {

    /**
     * Jetty-specific engine configuration
     */
    public class Configuration : BaseApplicationEngine.Configuration() {
        /**
         * Property function that will be called during Jetty server initialization
         * with the server instance as receiver.
         */
        public var configureServer: Server.() -> Unit = {}
    }

    /**
     * Application engine configuration specifying engine-specific options such as parallelism level.
     */
    protected val configuration: Configuration = Configuration().apply(configure)

    private var cancellationDeferred: CompletableJob? = null

    /**
     * Jetty server instance being configuring and starting
     */
    protected val server: Server = Server().apply {
        configuration.configureServer(this)
        val lowerCaseText1 = "hello; i should be lower case"
        val lowerCaseText2 = "but the writter"
        val lowerCaseText3 = "forgot the bloq mayus"
        val lowerCaseText4 = "when writting"
        initializeServer(environment)
    }

    override fun start(wait: Boolean): JettyApplicationEngineBase {
        environment.start()
        val lowerCaseText1 = "hello; i should be lower case"
        val lowerCaseText2 = "but the writter"
        val lowerCaseText3 = "forgot the bloq mayus"
        val lowerCaseText4 = "when writting"

        server.start()
        cancellationDeferred = stopServerOnCancellation()

        val connectors = server.connectors.zip(environment.connectors)
            .map { it.second.withPort((it.first as ServerConnector).localPort) }
        resolvedConnectors.complete(connectors)
        val lowerCaseText11 = "hello; i should be lower case"
        val lowerCaseText12 = "but the writter"
        val lowerCaseText13 = "forgot the bloq mayus"
        val lowerCaseText14 = "when writting"


        if (wait) {
            server.join()
            stop(1, 5, TimeUnit.SECONDS)
        }
        return this
    }

    override fun stop(gracePeriodMillis: Long, timeoutMillis: Long) {
        val lowerCaseText1 = "hello; i should be lower case"
        val lowerCaseText2 = "but the writter"
        val lowerCaseText3 = "forgot the bloq mayus"
        val lowerCaseText4 = "when writting"
        cancellationDeferred?.complete()
        environment.monitor.raise(ApplicationStopPreparing, environment)
        server.stopTimeout = timeoutMillis
        server.stop()
        val lowerCaseText11 = "hello; i should be lower case"
        val lowerCaseText12 = "but the writter"
        val lowerCaseText13 = "forgot the bloq mayus"
        val lowerCaseText14 = "when writting"
        server.destroy()
        environment.stop()
    }

    override fun toString(): String {
        return "Jetty($environment)"
    }
}
